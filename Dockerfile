
FROM node:lts as deps
WORKDIR /opt/tewi
COPY package.json package.json
RUN npm install --omit=dev
RUN mv node_modules /tmp/node_modules
RUN npm install

FROM node:lts as builder
WORKDIR /opt/tewi
COPY package.json package.json
COPY --from=deps /opt/tewi/node_modules ./node_modules
COPY src ./src
COPY .babelrc.json .
RUN npm run build

FROM node:lts as sysdeps
RUN apt update
RUN apt install -y ffmpeg 
RUN curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/bin/yt-dlp
RUN chmod a+rx /usr/bin/yt-dlp

FROM sysdeps
WORKDIR /opt/tewi
COPY --from=builder /opt/tewi/build ./build
COPY --from=builder /opt/tewi/package.json ./
COPY --from=deps /tmp/node_modules ./node_modules
COPY res ./res
COPY config ./config
COPY .git ./.git
RUN mkdir .data

CMD ["npm","start"]
