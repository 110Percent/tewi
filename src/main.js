import config from './config'
import messageResponses from './message-responses'
import commands from './commands'
import { run as runAutoruns } from './autoruns'
import { Client, Intents } from 'discord.js'
import { execSync } from 'child_process'
import { initGame } from './core/games/cs/main'

if (!config.token) {
  console.error('No token set! Please set a token in config/config.json')
  process.exit(1)
}

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_VOICE_STATES] })

client.once('ready', async () => {
  console.log('Ready!')
  const commitHash = execSync('git rev-parse HEAD').toString().trim().substring(0, 8)
  if (config.logGuild && config.logChannel) {
    const logChannel = await (await client.guilds.fetch(config.logGuild)).channels.fetch(config.logChannel)
    logChannel.send({ content: `Started on commit \`${commitHash}\`` })
  }
  initGame()
  runAutoruns(client).then()
})

client.on('messageCreate', async (msg) => {
  if (msg.author.bot) return
  setTimeout(async () => {
    msg = await msg.fetch()
    for (const [, resObject] of Object.entries(messageResponses)) {
      if (resObject.condition(msg, client)) {
        console.log(`Using message response ${resObject.name} in guild ${msg.guild}`)
        try {
          await resObject.action(msg, client)
        } catch (err) {
          console.error(err)
          if (config.logGuild && config.logChannel) {
            const logChannel = await (await client.guilds.fetch(config.logGuild)).channels.fetch(config.logChannel)
            logChannel.send({ content: `💥  **ERROR IN MESSAGE RESPONSE ** \`${resObject.name}\`\n\n\`\`\`\n${err}\n\`\`\`` })
          }
        }
      }
    }
  }, 200)
})

client.on('interactionCreate', async interaction => {
  if (!interaction.isCommand()) return

  if (interaction.commandName in commands) {
    console.log(`Using command ${interaction.commandName} in guild ${interaction.guild.name}`)
    try {
      await commands[interaction.commandName].action(interaction)
    } catch (err) {
      console.error(err)
      if (config.logGuild && config.logChannel) {
        const logChannel = await (await client.guilds.fetch(config.logGuild)).channels.fetch(config.logChannel)
        logChannel.send({ content: `💥  **ERROR IN COMMAND** \`${interaction.commandName}\`\n\n\`\`\`\n${err}\n\`\`\`` })
      }
    }
  }
})

client.on('interactionCreate', async interaction => {
  if (!interaction.isAutocomplete()) return

  const optName = interaction.options.getFocused(true).name
  let choices = commands[interaction.commandName].args.find(a => a.name === optName).choices(interaction)
  const optVal = interaction.options.getFocused(true).value
  if (optVal.length > 1) {
    choices = choices.filter(c => c.toLowerCase().includes(optVal.toLowerCase()))
  }
  choices = choices.slice(0, 25)
  console.log(`Returning autocomplete options for command ${interaction.commandName} option ${optName}`)
  try {
    interaction.respond(choices.map(c => {
      return { name: c, value: c }
    }))
  } catch (err) {
    console.error(err)
    if (config.logGuild && config.logChannel) {
      const logChannel = await (await client.guilds.fetch(config.logGuild)).channels.fetch(config.logChannel)
      logChannel.send({ content: `💥  **ERROR IN AUTOCOMPLETE** \`${interaction.commandName}\`\n\n\`\`\`\n${err}\n\`\`\`` })
    }
  }
})

client.login(config.token)
