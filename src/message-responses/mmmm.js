const regex = /^mm+$/gi

export default {
  name: 'mmmm',
  condition: (msg) => regex.test(msg.content),
  action: async (msg) => {
    await msg.react('999122626553327777')
  }
}
