export default {
  name: 'dog',
  condition: (msg) => (new RegExp(`(?:^|[^a-zA-Z])${Buffer.from('Z2F5', 'base64').toString()}(?:$|[^a-zA-Z])`)).test(msg.content),
  action: async (msg) => {
    await msg.react('990395456435667034')
  }
}
