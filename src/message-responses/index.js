import * as fs from 'fs'
import * as path from 'path'

const files = fs.readdirSync(__dirname).filter(f => f !== 'index.js')
const responseObjects = {}

for (const file of files) {
  const name = file.split('.')[0]
  responseObjects[name] = require(path.join(__dirname, file)).default
}

export default responseObjects
