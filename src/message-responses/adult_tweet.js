import { MessageEmbed } from 'discord.js'
import twitterClient from '../core/twitter_client'

const roClient = twitterClient.readOnly

export default {
  name: 'adult_twitter_embed',
  condition: (msg) => /^https:\/\/twitter\.com\/.+\/status\/\d+\S*$/g.test(msg.content),
  action: async (msg) => {
    setTimeout(() => handleMessage(msg), 1000)
  }
}

async function handleMessage (msg) {
  msg = await msg.fetch(true)
  if (msg.embeds.length > 0) return

  // TODO: CACHE THIS
  const tweetID = msg.content.split('/')[5].split('?')[0]
  try {
    const data = await roClient.v2.singleTweet(tweetID, {
      expansions: ['attachments.media_keys', 'author_id'],
      'media.fields': ['url'],
      'tweet.fields': ['public_metrics', 'created_at', 'possibly_sensitive'],
      'user.fields': ['profile_image_url']
    })

    // console.log(JSON.stringify(data, null, 4))
    if (data.data.possibly_sensitive) {
      const embed = new MessageEmbed()
        .setColor('#1da0f2')
        .setDescription(data.data.text)
        .setImage(data.includes.media[0].url)
        .setAuthor({
          name: data.includes.users[0].name,
          url: `https://twitter.com/${data.includes.users[0].username}`,
          iconURL: data.includes.users[0].profile_image_url
        })
        .addField('Likes', data.data.public_metrics.like_count.toString(), true)
        .addField('Retweets', data.data.public_metrics.retweet_count.toString(), true)
        .setTimestamp(new Date(data.data.created_at))
        .setFooter({ iconURL: 'https://abs.twimg.com/icons/apple-touch-icon-192x192.png', text: 'Twitter' })

      msg.reply({ embeds: [embed] })
    }
  } catch (err) {
    console.error(`Error fetching tweet ${msg.content}`)
    console.error(err)
  }
}
