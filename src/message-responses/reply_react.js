export default {
  name: 'reply_react',
  condition: (msg, client) => (msg.mentions.repliedUser?.equals(client.user) || msg.mentions?.has(client.user)) && msg.guild,
  action: async (msg) => {
    const emojis = await msg.guild.emojis.fetch()
    await msg.react(emojis.at(stringHashBound(msg.content, emojis.size, Number(msg.author.id))))
  }
}

function stringHashBound (str, num, initial) {
  return str.split('').map((c, i) => c.charCodeAt(0) * (i % 2 ? num - (num % i) : i)).reduce((a, b) => a + b, initial || 0) % num
}
