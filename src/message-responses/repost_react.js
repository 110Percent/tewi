import Keyv from 'keyv'

import axios from 'axios'
import phash from 'sharp-phash'

const urlList = new Keyv('sqlite://.data/tewi_data.sqlite', {
  namespace: 'guild_links', busyTimeout: 10000
})

export default {
  name: 'repost_react',
  condition: (msg) => 'embeds' in msg && msg.embeds.length > 0 && (msg.embeds[0].thumbnail || msg.embeds[0].image),
  action: async (msg) => {
    const imageURL = (msg.embeds[0].thumbnail ? msg.embeds[0].thumbnail : msg.embeds[0].image).proxyURL
    const res = await axios.get(imageURL, { responseType: 'arraybuffer' })
    const buffer = Buffer.from(res.data, 'base64')
    const hash = await phash(buffer)
    const found = await urlList.get(`${msg.channelId} ${hash}`)
    if (found) {
      await msg.react('♻')
    } else {
      await urlList.set(`${msg.channelId} ${hash}`, true)
    }
  }
}
