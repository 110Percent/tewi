import * as path from 'path'
import * as fs from 'fs'

const configPath = path.join(__dirname, '..', 'config', 'config.json')

if (!fs.existsSync(configPath)) {
  fs.copyFileSync(path.join(__dirname, '..', 'config', 'config.example.json'), configPath)
}

const config = require(configPath)
export default config
