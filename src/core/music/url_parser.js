import play from 'play-dl'
import { StreamType } from '@discordjs/voice'

const AUDIO_FILETYPES = {
  ogg: StreamType.OggOpus,
  webm: StreamType.WebmOpus,
  mp3: StreamType.Arbitrary,
  wav: StreamType.Arbitrary,
  flac: StreamType.Arbitrary
}

export async function parseURL (url) {
  if (play.yt_validate(url) === 'video') {
    const info = await play.video_info(url)
    return {
      title: info.video_details.title, audio: async () => await play.stream_from_info(info), url
    }
  } else if (await play.so_validate(url) === 'track') {
    const info = await play.soundcloud(url)
    return {
      title: info.name,
      audio: async () => await play.stream_from_info(info),
      url
    }
  } else {
    const type = AUDIO_FILETYPES[url.split('.')[url.split('.').length - 1]]
    if (!type) return null
    return {
      title: url.split('/')[url.split('/').length - 1],
      audio: async () => {
        return {
          stream: url,
          type,
          url
        }
      }
    }
  }
}
