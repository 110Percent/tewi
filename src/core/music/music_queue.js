import { parseURL } from './url_parser'
import { createAudioPlayer, createAudioResource, joinVoiceChannel, NoSubscriberBehavior } from '@discordjs/voice'
import play from 'play-dl'

export default class MusicQueue {
  constructor (mgr) {
    this.manager = mgr
    this.tracks = []
    this.playing = false
    this.player = createAudioPlayer({
      behaviors: {
        noSubscriber: NoSubscriberBehavior.Play
      }
    })
    this.channel = null
    this.connection = null
    console.log('New Music Queue')
  }

  joinChannel (channel) {
    this.channel = channel
    this.connection = joinVoiceChannel({
      channelId: channel.id, guildId: channel.guild.id, adapterCreator: channel.guild.voiceAdapterCreator
    })
    this.connection.subscribe(this.player)
  }

  getNextTrack () {
    return this.tracks[0]
  }

  cycleTracks () {
    this.tracks.splice(0, 1)
  }

  skip () {
    this.player.stop()
  }

  async startPlaying () {
    if (this.playing) return
    this.playing = true
    const audio = await this.getNextTrack().audio()
    this.player.play(createAudioResource(audio.stream, { inputType: audio.type }))

    this.player.addListener('idle', async () => {
      this.cycleTracks()
      if (this.getNextTrack()) {
        const audio = await this.getNextTrack().audio()
        this.player.play(createAudioResource(audio.stream, { inputType: audio.type }))
      } else {
        this.playing = false
        this.player.removeAllListeners()
        this.connection.destroy()
      }
    })
  }

  async searchTrack (query) {
    let link, title, result
    if (query.startsWith('https://')) {
      console.log('Link found!')
      if (play.yt_validate(query) === 'video') {
        console.log('Query is a YouTube video. Fetching information...')
        result = await this.enqueueURL(query)
      } else if (await play.so_validate(query) === 'track') {
        console.log('Query is a SoundCloud track. Fetching information...')
        result = await this.enqueueURL(query)
      } else if (play.sp_validate(query) === 'track') {
        console.log('Query is a Spotify track link. Fetching information...')
        const track = await play.spotify(query)
        const res = await this.spotifyTrackYoutube(track)
        title = res.title
        link = res.link
        result = await this.enqueueURL(link, title)
      } else if (play.sp_validate(query) === 'playlist') {
        console.log('Query is a Spotify playlist link. Fetching...')
        const playlist = await play.spotify(query)
        const tracks = await playlist.all_tracks()
        let track = await this.spotifyTrackYoutube(tracks[0])
        await this.enqueueURL(track.link, track.title);
        (async () => {
          for (let i = 1; i < tracks.length; i++) {
            track = await this.spotifyTrackYoutube(tracks[i])
            await this.enqueueURL(track.link, track.title)
          }
        })().then()
        title = playlist.name
        link = playlist.url
        result = { link, title }
      } else {
        result = await this.enqueueURL(query)
      }
    }
    if (!result) {
      console.log('No formats found; searching...')
      const res = await this.spotifyYoutube(query)
      title = res.title
      link = res.link
      result = await this.enqueueURL(link, title)
    }
    return result
  }

  async enqueueURL (url, title) {
    const track = await parseURL(url)
    if (title) track.title = title
    this.tracks.push(track)
    console.log(`Enqueued track ${track.title}`)
    return track
  }

  async spotifyTrackYoutube (track) {
    const ytQuery = `${track.artists[0].name} - ${track.name}`
    console.log(`Checking URL cache for ${ytQuery}`)
    let link = await this.manager.keyv.get(ytQuery)
    if (link) {
      console.log(`URL found: ${link}`)
      return { title: ytQuery, link }
    }
    console.log(`Searching YouTube for music matching ${ytQuery}`)
    const searched = await play.search(ytQuery, { limit: 5, source: { youtube: 'video' } })
    let maxScore = 0
    link = searched[0].url
    for (const searchResult of searched) {
      const lengthDiff = Math.abs(searchResult.durationInSec - track.durationInSec)
      const similarityScore = Math.log10(searchResult.views) / Math.log2(lengthDiff + 1.001)
      console.log(`${searchResult.title}\t-\tScore: ${similarityScore}`)
      if (similarityScore > maxScore && !searchResult.discretionAdvised) {
        try {
          await play.video_info(searchResult.url)
          maxScore = similarityScore
          link = searchResult.url
        } catch (e) {
          console.error(e)
        }
      }
    }
    console.log('Adding URL to cache...')
    await this.manager.keyv.set(ytQuery, link, 2628000)
    return { title: ytQuery, link }
  }

  async spotifyYoutube (query) {
    console.log(`Searching Spotify for music matching ${query}`)
    let results = []
    try {
      results = await play.search(query, { source: { spotify: 'track' } })
    } catch (err) {
      console.error(err)
    }
    let link
    let title
    if (results.length > 0) {
      const res = await this.spotifyTrackYoutube(results[0])
      link = res.link
      title = res.title
    } else {
      const ytQuery = results[0] ? `${results[0].artists[0].name} - ${results[0].name}` : query
      console.log(`Searching YouTube for music matching ${ytQuery}`)
      const searched = await play.search(ytQuery, { limit: 5, source: { youtube: 'video' } })

      for (const s of searched) {
        try {
          title = (await play.video_info(s.url)).video_details.title
          link = s.url
          break
        } catch (e) {
          console.error(e)
        }
      }
    }
    return { title, link }
  }
}
