import MusicQueue from './music_queue'
import play from 'play-dl'
import Keyv from 'keyv'

soundCloudToken().then()

async function soundCloudToken () {
  console.log('Setting SoundCloud free client ID')
  const id = await play.getFreeClientID()
  await play.setToken({
    soundcloud: {
      client_id: id
    }
  })
}

class QueueManager {
  constructor () {
    this.queues = {}
    this.keyv = new Keyv('sqlite://.data/tewi_data.sqlite', {
      table: 'song_urls', busyTimeout: 10000
    })
    this.fetchLoopsStarted = false
  }

  get (guildID) {
    if (!this.fetchLoopsStarted) {
      this.fetchLoopsStarted = true
      setInterval(soundCloudToken, 3600000)
      setInterval(async () => {
        if (play.is_expired()) {
          console.log('Spotify token expired. Refreshing...')
          await play.refreshToken()
        }
      }, 5000)
    }
    if (!this.queues[guildID]) this.queues[guildID] = new MusicQueue(this)
    return this.queues[guildID]
  }
}

const mgr = new QueueManager()
export default mgr
