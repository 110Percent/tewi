import { TwitterApi } from 'twitter-api-v2'
import config from '../config'

const twitterClient = new TwitterApi(config.twitterBearerToken)

export default twitterClient
