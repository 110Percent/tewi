import sqlite3 from 'better-sqlite3'

const db = sqlite3('.data/tewi_data.sqlite')

function initializeDatabase () {
  const createDbScript = 'CREATE TABLE IF NOT EXISTS game_money (id INTEGER PRIMARY KEY, money INTEGER);'
  db.exec(createDbScript)
}

export function initGame () {
  initializeDatabase()
}
