import sqlite3 from 'better-sqlite3'

const db = sqlite3('.data/tewi_data.sqlite')

async function run (client) {
  setTimeout(() => {
    db.exec('UPDATE game_money SET money = money + 50')
  }, 3600000)

  client.on('messageCreate', async (msg) => {
    if (msg.author.bot) return
    const exists = Object.values(db.prepare(`SELECT EXISTS(SELECT 1 FROM game_money WHERE id = ${msg.author.id})`).get())[0]
    if (exists) {
      db.exec(`UPDATE game_money SET money = money + 1 WHERE id = ${msg.author.id}`)
    } else {
      db.exec(`INSERT INTO game_money (id, money) VALUES (${msg.author.id}, 1)`)
    }
  })
}

export default {
  name: 'givemoney', run
}
