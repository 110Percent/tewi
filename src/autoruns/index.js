import * as fs from 'fs'
import * as path from 'path'

const files = fs.readdirSync(__dirname).filter(f => f !== 'index.js')
const autorunObjects = {}

for (const file of files) {
  const name = file.split('.')[0]
  autorunObjects[name] = require(path.join(__dirname, file)).default
}

export async function run (client) {
  for (const autorun of Object.values(autorunObjects)) {
    console.log(`Running autorun ${autorun.name}`)
    autorun.run(client)
  }
}
