const { SlashCommandBuilder } = require('@discordjs/builders')
const { REST } = require('@discordjs/rest')
const { Routes } = require('discord-api-types/v9')
const path = require('path')
const config = require(path.join(__dirname, '..', 'config', 'config.json'))
const commands = require('./commands').default;

(async () => {
  const cmdList = []
  for (const cmdObjRaw of Object.values(commands)) {
    let cmdObj = new SlashCommandBuilder().setName(cmdObjRaw.name).setDescription(cmdObjRaw.description).setDefaultPermission(true)
    if (cmdObjRaw.args) {
      for (const argRaw of cmdObjRaw.args) {
        if (argRaw.type === 'string') {
          cmdObj = cmdObj.addStringOption(option => {
            option.setName(argRaw.name).setDescription(argRaw.description).setRequired(argRaw.required)
            if (argRaw.choices && !argRaw.autocomplete) {
              option = option.addChoices(argRaw.choices().map(choice => [choice, choice]))
            }
            return option
          })
        } else if (argRaw.type === 'number') {
          cmdObj = cmdObj.addNumberOption(option => {
            option.setName(argRaw.name).setDescription(argRaw.description).setRequired(!!argRaw.required)
            return option
          })
        }
      }
    }
    const jsonObj = cmdObj.toJSON()
    if (cmdObjRaw.args) {
      for (let i = 0; i < cmdObjRaw.args.length; i++) {
        jsonObj.options[i].autocomplete = !!cmdObjRaw.args[i].autocomplete
      }
    }
    cmdList.push(jsonObj)
  }

  const rest = new REST({ version: '9' }).setToken(config.token)

  if (config.globalCommands) {
    await rest.put(Routes.applicationCommands(config.clientID), { body: cmdList })
  } else {
    await rest.put(Routes.applicationGuildCommands(config.clientID, config.guildID), { body: cmdList })
  }
  console.log('Successfully registered application commands.')
})()
