export default {
  name: 'stealemoji',
  description: 'Steal an emoji and add it to the current server.',
  args: [{
    name: 'emoji', type: 'string', description: 'The emoji to steal', required: true
  }],
  action: async (interaction) => {
    const value = interaction.options.get('emoji').value
    const matches = value.match(/<:(.+):(\d+)>/)
    if (matches.length < 1) {
      interaction.reply('No emoji found!')
      return
    }

    const name = matches[1].toString()
    const emojiId = matches[2]
    const attachment = `https://cdn.discordapp.com/emojis/${emojiId}.png`
    try {
      const emoji = await interaction.guild.emojis.create(attachment, name)
      await interaction.reply(emoji.toString())
    } catch (err) {
      await interaction.reply('Couldn\'t add emoji')
    }
  }
}
