import * as path from 'path'
import { MessageAttachment } from 'discord.js'
import * as fs from 'fs'
import YTDlpWrap from 'yt-dlp-wrap'

const ytDlpWrap = new YTDlpWrap()

export default {
  name: 'tok2vid',
  description: 'Download a post from TikTok and post it as a video.',
  args: [{
    name: 'url', type: 'string', description: 'TikTok post URL', required: true
  }],
  action: async (interaction) => {
    const url = interaction.options.get('url').value
    if (!url.startsWith('https://vm.tiktok.com') && !url.startsWith('https://www.tiktok.com/')) {
      await interaction.reply({
        content: 'Invalid TikTok link! Must start with `https://vm.tiktok.com` or `https://www.tiktok.com`',
        ephemeral: true
      })
      return
    }

    await interaction.deferReply()

    const outPath = path.join(__dirname, '..', '..', 'tmp', `${interaction.id}.mp4`)
    await ytDlpWrap.execPromise([url, '-S', '+vcodec', '-o', outPath])

    const file = new MessageAttachment(outPath)
    await interaction.editReply({ files: [file] })
    fs.rm(outPath, () => { })
  }
}
