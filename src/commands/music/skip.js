import queueManager from '../../core/music/queue_manager'

export default {
  name: 'skip',
  description: 'Skip the current song in the music queue.',
  action: async (interaction) => {
    const queue = queueManager.get(interaction.guild.id)
    const track = queue.getNextTrack()
    queue.skip()
    await interaction.reply(`Skipped \`${track.title}\``)
  }
}
