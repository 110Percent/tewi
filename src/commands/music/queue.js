import queueManager from '../../core/music/queue_manager'
import { MessageEmbed } from 'discord.js'

export default {
  name: 'queue',
  description: 'Show the upcoming tracks in the music queue.',
  action: async (interaction) => {
    const queue = queueManager.get(interaction.guild.id)
    if (queue.getNextTrack()) {
      const embed = new MessageEmbed()
        .setTitle('Music Queue')
        .setDescription(queue.tracks.map((t, i) => `${i + 1}. [${t.title}](${t.url})`).join('\n'))
      await interaction.reply({ embeds: [embed] })
    } else {
      await interaction.reply('Nothing is playing!')
    }
  }
}
