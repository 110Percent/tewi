import queueManager from '../../core/music/queue_manager'
import { MessageEmbed } from 'discord.js'

export default {
  name: 'play',
  description: 'Play a YouTube video in your current voice channel.',
  args: [{
    name: 'query', type: 'string', description: 'Search query', required: true
  }],
  action: async (interaction) => {
    if (!interaction.member.voice.channelId) {
      await interaction.reply('Not in a voice channel!')
      return
    }

    if (!interaction.member.voice.channel.joinable) {
      await interaction.reply('Cannot join that voice channel!')
      return
    }

    const queue = queueManager.get(interaction.guild.id)

    await interaction.deferReply()
    const track = await queue.searchTrack(interaction.options.get('query').value)

    queue.joinChannel(interaction.member.voice.channel)
    await queue.startPlaying()
    const embed = new MessageEmbed()
      .setTitle('Enqueued!')
      .setDescription(`[${track.title}](${track.url})`)
    await interaction.editReply({ embeds: [embed] })
  }
}
