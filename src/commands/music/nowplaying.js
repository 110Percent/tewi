import queueManager from '../../core/music/queue_manager'
import { MessageEmbed } from 'discord.js'

export default {
  name: 'nowplaying',
  description: 'Show the current track in the music queue.',
  action: async (interaction) => {
    const queue = queueManager.get(interaction.guild.id)
    const track = queue.getNextTrack()
    if (track) {
      const embed = new MessageEmbed()
        .setTitle('Now playing!')
        .setDescription(`[${track.title}](${track.url})`)
      await interaction.reply({ embeds: [embed] })
    } else {
      await interaction.reply('Nothing is playing!')
    }
  }
}
