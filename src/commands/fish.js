import axios from 'axios'
import * as cheerio from 'cheerio'
import { sleep } from '../core/utils'
import { MessageEmbed } from 'discord.js'

// Get fish names
let allFishes = []
axios.get('https://en.wikipedia.org/wiki/List_of_fish_common_names').then(r => {
  const $ = cheerio.load(r.data)
  allFishes = $('.div-col li a').toArray().map(link => {
    return {
      url: link.attribs.href,
      name: link.attribs.title.split(' (')[0]
    }
  })
})

export default {
  name: 'fish',
  description: 'Go fishing.',
  action: async (interaction) => {
    await interaction.reply('You cast your rod...')
    await sleep(Math.floor(Math.random() * 16000 + 4000))
    await interaction.editReply('Something stirs...')
    await sleep(Math.floor(Math.random() * 6000 + 1000))
    const fish = allFishes[Math.floor(Math.random() * allFishes.length)]

    let embed = new MessageEmbed().setDescription(`🎣 You caught a ${fish.name}!`)

    const res = await axios.get('https://en.wikipedia.org' + fish.url)
    const fish$ = cheerio.load(res.data)
    const imgElements = fish$('img.mw-file-element')
    if (imgElements.length > 0) {
      embed = embed.setImage('https:' + imgElements[0].attribs.src)
    }

    await interaction.editReply({ content: ' ', embeds: [embed] })
  }
}
