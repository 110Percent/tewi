import sharp from 'sharp'
import axios from 'axios'
import { MessageAttachment } from 'discord.js'
import config from '../config'

export default {
  name: 'caption',
  description: 'Add a caption to a previous image.',
  args: [{
    name: 'caption', type: 'string', description: 'Caption to add', required: true
  }],
  action: async (interaction) => {
    const messages = await interaction.channel.messages.fetch({ limit: config.msgScanCount || 25 })
    let imageURL = null
    for (const msg of messages.values()) {
      for (const embed of msg.embeds) {
        if (embed.type === 'image') {
          imageURL = embed.thumbnail.proxyURL
          break
        }
      }
      if (imageURL) break
      for (const attachment of msg.attachments.values()) {
        if (attachment.contentType.startsWith('image/')) {
          imageURL = attachment.proxyURL
          break
        }
      }
      if (imageURL) break
    }
    if (!imageURL) {
      await interaction.reply('No image found within the past 10 messages!')
      return
    }
    await interaction.deferReply()
    const textLines = ['']
    const words = interaction.options.get('caption').value.split(' ')
    for (const word of words) {
      if (textLines[textLines.length - 1].length + word.length > 16) {
        textLines.push(word)
      } else {
        textLines[textLines.length - 1] = textLines[textLines.length - 1] + ' ' + word
      }
    }
    const res = await axios.get(imageURL, { responseType: 'arraybuffer' })
    const buffer = Buffer.from(res.data, 'base64')
    const resized = await sharp(await sharp(buffer)
      .resize({ width: 640, fit: sharp.fit.inside }).toBuffer())
    const meta = await resized.metadata()
    const resolution = meta.height / 240
    const img = await sharp(await resized.extend({ top: Math.ceil(48 * (textLines.length + 1) * (resolution > 3 ? 3 : resolution)), background: 'white' }).toBuffer())
    const svgImage = `
    <svg width="${meta.width}" height="${(48 * (textLines.length) + 24) * (resolution > 3 ? 3 : resolution)}">
      <style>
      .caption {
        color: black;
        font-size: ${32 * (resolution > 3 ? 3 : resolution)}px;
        font-family: Impact, Lucida Grande, Verdana, Geneva, Arial, sans-serif;
      }
      </style>
      ${textLines.map((line, i) =>
      `<text x="50%" y="${(i + 1.5) / (textLines.length + 1) * 100}%" text-anchor="middle" class="caption">${line}</text>`).join('\n')}
    </svg>
    `
    const svgBuffer = Buffer.from(svgImage)
    const imgBuf = await img
      .composite([
        {
          input: svgBuffer,
          top: 0,
          left: 0
        }
      ])
      .toBuffer()
    await interaction.editReply({ files: [new MessageAttachment(imgBuf)] })
  }
}
