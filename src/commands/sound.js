import { AudioPlayerStatus, createAudioPlayer, createAudioResource, joinVoiceChannel } from '@discordjs/voice'
import * as path from 'path'
import * as fs from 'fs'

export default {
  name: 'sound',
  description: 'Play a sound in your current voice channel.',
  args: [{
    name: 'sound',
    type: 'string',
    description: 'Sound to play',
    required: true,
    autocomplete: true,
    choices: () => fs.readdirSync(path.join(__dirname, '..', '..', 'res', 'sound')).filter(fn => fn.endsWith('.ogg')).map(fn => fn.split('.')[0])
  }],
  action: async (interaction) => {
    if (!interaction.member.voice.channelId) {
      await interaction.reply('Not in a voice channel!')
      return
    }

    if (!interaction.member.voice.channel.joinable) {
      await interaction.reply('Cannot join that voice channel!')
      return
    }

    const channel = interaction.member.voice.channel
    const connection = joinVoiceChannel({
      channelId: channel.id, guildId: channel.guild.id, adapterCreator: channel.guild.voiceAdapterCreator
    })

    const player = createAudioPlayer()
    const sound = createAudioResource(path.join(__dirname, '..', '..', 'res', 'sound', `${interaction.options.get('sound').value}.ogg`))

    interaction.reply(`Playing sound \`${interaction.options.get('sound').value}\``)
    player.play(sound)
    connection.subscribe(player)

    player.on(AudioPlayerStatus.Idle, async () => {
      connection.destroy()
    })
  }
}
