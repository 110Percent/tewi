import { exec } from 'child_process'
import * as path from 'path'
import { MessageAttachment } from 'discord.js'
import * as fs from 'fs'

import config from '../config'

export default {
  name: 'vid2gif',
  description: 'Convert a video from the past 10 messages to a gif.',
  action: async (interaction) => {
    const messages = await interaction.channel.messages.fetch({ limit: config.msgScanCount || 25 })
    let video = null
    for (const msg of messages.values()) {
      for (const embed of msg.embeds) {
        if (embed.video) {
          video = embed.video.proxyURL
          break
        }
      }
      if (video) break
      for (const attachment of msg.attachments.values()) {
        if (attachment.contentType.startsWith('video/')) {
          video = attachment.proxyURL
          break
        }
      }
      if (video) break
    }
    if (!video) {
      await interaction.reply('No video found within the past 10 messages!')
      return
    }
    await interaction.deferReply()

    const width = 320
    const fps = 10
    const outPath = path.join(__dirname, '..', '..', 'tmp', `${interaction.id}.gif`)
    const videob64 = btoa(video)
    const command = ['ffmpeg', '-y', '-i', `$(echo ${videob64} | base64 -d)`, '-filter_complex', `"fps=${fps},scale=${width}:-1:flags=lanczos,split=2 [a][b]; [a] palettegen [pal]; [b] fifo [b]; [b] [pal] paletteuse"`, outPath].join(' ')
    await new Promise((resolve, reject) => {
      exec(command, (error, stdout, stderr) => {
        if (error) reject(stderr); else resolve(stdout)
      })
    })

    const file = new MessageAttachment(outPath)
    await interaction.editReply({ files: [file] })
    fs.rm(outPath, () => {})
  }
}
