import axios from 'axios'

const endpoint = 'https://en.wikipedia.org/w/api.php?action=opensearch&format=json&formatversion=2&search=%s&namespace=0&limit=5'

export default {
  name: 'wikipedia',
  description: 'Searches wikipedia for an article.',
  args: [{
    name: 'query',
    type: 'string',
    description: 'What to search',
    required: true,
    autocomplete: false
  }],
  action: async (interaction) => {
    const query = interaction.options.get('query').value
    const res = await axios.get(endpoint.replace('%s', encodeURIComponent(query)))
    if (res.data[1].length < 1) {
      interaction.reply(`No Wikipedia page found for \`${query}\`.`)
      return
    }

    let point = res.data[1].findIndex(i => i.toLowerCase() === query.toLowerCase())
    if (!point) {
      point = 0
    }
    const url = res.data[res.data.length - 1][point]
    interaction.reply(url)
  }
}
