import config from '../config'
import axios from 'axios'
import { MessageEmbed } from 'discord.js'


export default {
  name: 'inquire',
  description: 'Say something to Tewi.',
  args: [{
    name: 'inquiry',
    type: 'string',
    description: 'What to say to Tewi',
    required: true,
    autocomplete: false
  }],
  action: async (interaction) => {
    await interaction.deferReply();

    const msgContent = `[${interaction.member.displayName}] ${interaction.options.get('inquiry').value}` 
	  console.log(msgContent)
    const ollamaRes = await axios({
      method: 'POST',
      url: `${config.ollamaEndpoint}/api/chat`,
      data: {
        model: config.ollamaModel,
        stream: false,
        messages: [
	  {role: 'user', content: `${msgContent}\nbe brief.`}
	]
      }
    })

    if (ollamaRes.status !== 200) {
      await interaction.reply({ content: 'Sorry, I couldn\'t think of a response...'})
    } else {
      await interaction.editReply({ content: ollamaRes.data.message.content})
    }
  }
}
