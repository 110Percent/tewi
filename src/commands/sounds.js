import sound from './sound'
import { MessageEmbed } from 'discord.js'

export default {
  name: 'sounds',
  description: 'List available sounds.',
  action: async (interaction) => {
    const sounds = sound.args[0].choices()
    const embed = new MessageEmbed().setTitle('Available Sounds').setDescription(sounds.map(s => `\`${s}\``).join(', '))
    await interaction.reply({ embeds: [embed], ephemeral: true })
  }
}
