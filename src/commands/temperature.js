export default {
  name: 'temperature',
  description: 'Convert between temperatures.',
  args: [{
    name: 'celsius', type: 'number', description: 'Temperature in celsius', autocomplete: false
  }, {
    name: 'fahrenheit', type: 'number', description: 'Temperature in fahrenheit', autocomplete: false
  }, {
    name: 'kelvin', type: 'number', description: 'Temperature in kelvin', autocomplete: false
  }],
  action: async (interaction) => {
    const temps = [0, 0, 0]
    if (interaction.options.get('celsius')) {
      temps[0] = interaction.options.get('celsius').value
      temps[1] = temps[0] * 1.8 + 32
      temps[2] = temps[0] + 273.15
    } else if (interaction.options.get('kelvin')) {
      temps[2] = interaction.options.get('kelvin').value
      temps[0] = temps[2] - 273.15
      temps[1] = temps[0] * 1.8 + 32
    } else if (interaction.options.get('fahrenheit')) {
      temps[1] = interaction.options.get('fahrenheit').value
      temps[0] = (temps[1] - 32) / 1.8
      temps[2] = temps[0] + 273.15
    } else {
      await interaction.reply('No temperature values provided!')
      return
    }

    temps[0] = Math.floor(temps[0] * 100) / 100
    temps[1] = Math.floor(temps[1] * 100) / 100
    temps[2] = Math.floor(temps[2] * 100) / 100

    await interaction.reply(`**${temps[0]}°C** = **${temps[1]}°F** = **${temps[2]}K**`)
  }
}
