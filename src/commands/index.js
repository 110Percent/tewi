import * as fs from 'fs'
import * as path from 'path'

const cmdObjects = {}

const parseDirectory = (pathName) => {
  const files = fs.readdirSync(pathName).filter(f => f !== 'index.js').map(f => path.join(pathName, f))
  for (const file of files) {
    if (fs.statSync(file).isDirectory()) {
      parseDirectory(file)
    } else if (file.endsWith('.js')) {
      const cmdObject = require(file).default
      cmdObjects[cmdObject.name] = cmdObject
    }
  }
}

parseDirectory(__dirname)

export default cmdObjects
