export default {
  name: 'ping',
  description: 'Gives a simple reply.',
  action: async (interaction) => {
    const time = new Date().getTime()
    await interaction.reply('Pong!')
    await interaction.editReply(`Pong!\nResponse time: ${time - interaction.createdTimestamp}ms`)
  }
}
