import Keyv from 'keyv'
import { getTags } from './tags'

const keyv = new Keyv('sqlite://.data/tewi_data.sqlite', {
  table: 'tags'
})

export default {
  name: 'deletetag',
  description: 'Delete a tag.',
  args: [{
    name: 'tag',
    type: 'string',
    description: 'Tag to delete',
    required: true,
    autocomplete: true,
    choices: (interaction) => getTags(interaction.guild.id)
  }],
  action: async (interaction) => {
    const key = `${interaction.guild.id}:${interaction.options.get('tag').value}`
    const hasTag = await keyv.has(key)
    if (!hasTag) {
      await interaction.reply(`Tag ${interaction.options.get('tag').value()} does not exist for this guild.`)
      return
    }

    await keyv.delete(key)
    await interaction.reply(`Tag \`${interaction.options.get('tag').value}\` deleted.`)
  }
}
