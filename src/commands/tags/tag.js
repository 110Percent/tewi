import Keyv from 'keyv'
import { getTags } from './tags'

const keyv = new Keyv('sqlite://.data/tewi_data.sqlite', {
  table: 'tags'
})

export default {
  name: 'tag',
  description: 'Send a tag to the chat.',
  args: [{
    name: 'tag',
    type: 'string',
    description: 'Tag to send',
    required: true,
    autocomplete: true,
    choices: (interaction) => getTags(interaction.guild.id)
  }],
  action: async (interaction) => {
    const key = `${interaction.guild.id}:${interaction.options.get('tag').value}`
    const hasTag = await keyv.has(key)
    if (!hasTag) {
      await interaction.reply(`Tag ${interaction.options.get('tag').value()} does not exist for this guild.`)
      return
    }

    const content = await keyv.get(key)
    await interaction.reply(content)
  }
}
