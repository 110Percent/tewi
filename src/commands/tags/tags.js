import { MessageEmbed } from 'discord.js'
import sqlite3 from 'better-sqlite3'

const db = sqlite3('.data/tewi_data.sqlite')

export default {
  name: 'tags',
  description: 'List available tags for this guild.',
  action: async (interaction) => {
    const tags = getTags(interaction.guild.id)
    const embed = new MessageEmbed().setTitle('Available Tags').setDescription(tags.map(s => `\`${s}\``).join(', '))
    await interaction.reply({ embeds: [embed], ephemeral: true })
  }
}

export function getTags (guildId) {
  const tags = db.prepare(`SELECT key FROM tags WHERE key LIKE 'keyv:${guildId}:%'`).all()
  return tags.map(t => splitKey(t.key))
}

function splitKey (key) {
  const newKey = key.split(':')
  newKey.splice(0, 2)
  return newKey.join(':')
}
