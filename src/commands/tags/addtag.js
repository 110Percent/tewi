import Keyv from 'keyv'

const keyv = new Keyv('sqlite://.data/tewi_data.sqlite', {
  table: 'tags'
})

export default {
  name: 'addtag',
  description: 'Add a tag that can be sent later.',
  args: [{
    name: 'name',
    type: 'string',
    description: 'The tag\'s name',
    required: true
  }, {
    name: 'content',
    type: 'string',
    description: 'The tag\'s content',
    required: true
  }],
  action: async (interaction) => {
    const name = interaction.options.get('name').value
    const hasTag = await keyv.has(`${interaction.guild.id}:${name}`)
    if (hasTag) {
      await interaction.reply(`Tag \`${name}\` already exists for this guild.`)
      return
    }

    await keyv.set(`${interaction.guild.id}:${name}`, interaction.options.get('content').value)
    await interaction.reply(`Created a new tag \`${name}\``)
  }
}
