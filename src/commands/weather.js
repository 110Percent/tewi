import config from '../config'
import axios from 'axios'
import { MessageEmbed } from 'discord.js'
import Keyv from 'keyv'

const keyv = new Keyv('sqlite://.data/tewi_data.sqlite', { table: 'city_data' })

export default {
  name: 'weather',
  description: 'Look up the weather in a city or a lat/long coordinate pair.',
  args: [{
    name: 'city',
    type: 'string',
    description: 'City Name (can also specify state/country codes)',
    autocomplete: false,
    required: true
  }],
  action: async (interaction) => {
    const apiKey = config.openWeatherMapAPIKey
    const searchCityName = interaction.options.get('city').value

    let cityName, lat, lon

    const cached = await keyv.get(searchCityName.toLowerCase())
    if (cached) {
      console.log(`Using cached coordinates for ${searchCityName}`);
      ({ cityName, lat, lon } = cached)
    } else {
      console.log(`Using geocoding API for ${searchCityName}`)
      const geoURL = `https://api.openweathermap.org/geo/1.0/direct?q=${searchCityName}&limit=1&appid=${apiKey}`
      const geoData = (await axios.get(geoURL)).data
      if (geoData.length < 1) {
        await interaction.reply(`Could not find a city named \`${searchCityName}\``)
        return
      }
      cityName = geoData[0].name + (geoData[0].state ? `, ${geoData[0].state}` : '') + (geoData[0].country ? `, ${geoData[0].country}` : '')
      lat = geoData[0].lat
      lon = geoData[0].lon

      await keyv.set(searchCityName.toLowerCase(), { cityName, lat, lon })
    }

    const weatherURL = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${apiKey}`
    const weatherData = (await axios.get(weatherURL)).data

    const temp = `${Math.floor(weatherData.main.temp - 271)}°C  (${Math.floor((weatherData.main.temp - 271) * 1.8 + 32)}°F)`
    const feelsLike = `${Math.floor(weatherData.main.feels_like - 271)}°C  (${Math.floor((weatherData.main.feels_like - 271) * 1.8 + 32)}°F)`
    const desc = `${temp}, ${weatherData.weather[0].main}\n\nFeels Like ${feelsLike}`

    const embed = new MessageEmbed().setTitle(`Weather for ${cityName}`)
      .setDescription(desc)
      .setThumbnail(`http://openweathermap.org/img/wn/${weatherData.weather[0].icon}@2x.png`)
      .setURL(`https://openweathermap.org/city/${weatherData.id}`)
    await interaction.reply({ embeds: [embed] })
  }
}
