import axios from 'axios'
import * as cheerio from 'cheerio'
import { MessageEmbed } from 'discord.js'

export default {
  name: 'define',
  description: 'Fetch a word\'s definition from The Free Dictionary.',
  args: [{
    name: 'word', type: 'string', description: 'Word to define', required: true
  }],
  action: async (interaction) => {
    const endpoint = 'https://www.thefreedictionary.com/'
    const userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36'

    if (interaction.options.get('word').value.length > 100) {
      await interaction.reply('Maximum length of 100 letters.')
    }

    const url = endpoint + interaction.options.get('word').value

    const res = await axios.get(url, {
      headers: {
        'User-Agent': userAgent
      }
    })

    const $ = cheerio.load(res.data)

    const matches = []
    const texts = ($('body').find('.ds-list').map(function () {
      return $(`<div>${$(this).html().replace(/<i>(.*?)<\/i>/g, (m, p1) => `*${p1}*`)}</div>`).text().trim()
    }))

    const title = $('h1').text()

    for (const text of texts) {
      if (text.startsWith((matches.length + 1) + '.') || matches.length < 1) {
        matches.push(text.replace(/^\d+\. (?:[a-z]\. })? ?/g, ''))
      } else {
        break
      }
    }

    let definitionEmbed

    if (matches.length > 0) {
      definitionEmbed = new MessageEmbed()
        .setColor('#086B9C')
        .setTitle(title)
        .setURL(url)
        .setAuthor({ name: 'The Free Dictionary', iconURL: 'https://img.tfd.com/touch/iphone-r.png', url: endpoint })
        .setDescription(matches.map((m) => `• ${m}`).join('\n'))
    } else {
      definitionEmbed = new MessageEmbed()
        .setColor('#d6394a')
        .setAuthor({ name: 'The Free Dictionary', iconURL: 'https://img.tfd.com/touch/iphone-r.png', url: endpoint })
        .setDescription('No matches found.')
    }

    await interaction.reply({ embeds: [definitionEmbed] })
  }
}
